#-------------------------------------------------
#
# Project created by QtCreator 2018-03-22T14:30:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = para
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    pathdialog.cpp

HEADERS  += mainwindow.h \
    pathdialog.h

FORMS    += mainwindow.ui \
    pathdialog.ui
